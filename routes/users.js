var router = express.Router();
var async = require('async');
router.post('/userFileUpload', function (req, res) {
    var email = req.body.email;
    var manValues = [email];
    async.waterfall([
        function (callback) {
            commonFunction.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {
            commonFunction.checkEmailAvailability(res, email, callback);

        }], function (updatePopup) {
        commonFunction.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {

            var sql = "UPDATE `tb_users` SET `file_path`=? WHERE `email`=? LIMIT 1"

            connection.query(sql, [result_file, email], function (err, result) {

                if (err) {

                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    sendResponse.successStatusMsg(res);
                }
            });


        });
    });
});
module.exports = router;