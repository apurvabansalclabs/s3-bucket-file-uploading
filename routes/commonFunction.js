var fs = require('node-fs');
var AWS = require('aws-sdk');
exports.checkBlank = function (arr) {
    return (checkBlank(arr));
};

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === '' || arr[i] === undefined || arr[i] === '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
};
exports.checkEmailAvailability = function (res, email, callback) {

    var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=? limit 1";
    connection.query(sql, [email], function (err, response) {

        if (err) {

            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {

            callback(null);
        }
        else {
            sendResponse.emailNotRegistered(res);

        }
    });
}

exports.uploadFile = function (file, folder, res, callback) {
    var filename = file.name;
    var path = file.path;
    var mimeType = file.type;

    fs.readFile(path, function (error, file_buffer) {
        if (error) {
            sendResponse.somethingWentWrongError(res);
        }

        filename = file.name;
        remotePath = config.get("production").Url + filename;
        filePath =config.get("production").Url;
        if (remotePath !== filePath) {
            AWS.config.update({
                accessKeyId: config.get('production').awsAccessKey,
                secretAccessKey: config.get('production').awsSecretKey
            });
            var s3bucket = new AWS.S3();
            var params = {
                Bucket: config.get('production').awsBucket,
                Key: folder + '/' + filename,
                Body: file_buffer,
                ACL: 'public-read',
                ContentType: 'mimetype'
            };

            s3bucket.putObject(params, function (err, data) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {

                    return callback(remotePath);

                }
            });
        }
        else {
            sendResponse.fileNotSelected(res);
        }
    });
};
