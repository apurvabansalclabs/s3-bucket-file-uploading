var mysql = require('mysql');
connection = mysql.createConnection({
    host: config.get("Database_Settings").host,
    user: config.get("Database_Settings").user,
    database: config.get("Database_Settings").database
});
connection.connect();