process.env.NODE_ENV = 'localDevelopment';
express = require("express");
var http = require('http');
var path = require('path');
config = require('config');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var connection = require('./routes/mySqlLib');
var multipartMiddleware = multipart();
var app = express();
sendResponse = require('./routes/sendResponse');
commonFunction = require('./routes/commonFunction');


var fileUpload = require('./routes/users');
var index = require('./routes/index');


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', index);
app.post('/userFileUpload', multipartMiddleware);
app.post('/userFileUpload', fileUpload);


http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Server listening on port " + config.get('PORT'));
});
